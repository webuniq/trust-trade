import React from 'react';

export default function DashboardFooter() {
    return (
        <div className="footer-bar">
            <div className="container">
                <div className="row">
                    <div className="col-md-7">
                        <span className="footer-copyright">Copyright 2018, <a href="#">ICO Crypto</a>.  All Rights Reserved.</span>
                    </div>
                    <div className="col-md-5 text-md-right">
                        <ul className="footer-links">
                            <li><a href="policy.html">Privacy Policy</a></li>
                            <li><a href="policy.html">Terms of Sales</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    )
}
